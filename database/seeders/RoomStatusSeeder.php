<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['status_name' => 'clean', 'color' => 'green'],
            ['status_name' => 'occupied', 'color' => 'red'],
            ['status_name' => 'dirty', 'color' => 'yellow'],
        ];
        DB::table('room_status')->insert($statuses);
    }
}
